import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";

const App = () => (
    <div>
        <Pesquisa />
        <BrandChoice />
        <Header />
        <AreasNegocio />
        <BrandschoiceMobile />
        <Menu />
        {/* <Page /> */}
        {/* <Footer/> */}
    </div>
);

const Pesquisa = () => (
    <section id="pesquisa">
        <div id="pesquisa_in">
            <form id="pesquisa_form" method="GET" name="pesquisa" action="https://www.loba.pt/pt/">
                <input type="hidden" name="op" value="search" />
                <label for="pesquisar_inp">termo a pesquisar</label>
                <input type="text" placeholder="termo a pesquisar" id="pesquisar_inp" name="q" />
            </form>
        </div>
    </section>
);

const BrandChoice = () => (
    <div id="brand_choice" class="">
        <div class="background"></div>
        <div class="content">
            <button></button>
            <ul>
                <li class="brands selected">
                    <a href="https://www.loba.pt/pt/brands" title="Brands">
                        <div class="logo_container">
                            <span class="white"></span>
                            <span class="over"></span>
                        </div>
                    </a>
                </li>
                <li class="ecommerce">
                    <a href="https://www.loba.pt/pt/ecommerce" title="Ecommerce">
                        <div class="logo_container">
                            <span class="white"></span>
                            <span class="over"></span>
                        </div>
                    </a>
                </li>
                <li class="applications">
                    <a href="https://www.loba.pt/pt/applications" title="APPLICATIONS">
                        <div class="logo_container">
                            <span class="white"></span>
                            <span class="over"></span>
                        </div>
                    </a>
                </li>
                <li class="productdesign">
                    <a href="https://www.loba.pt/pt/product-design" title="Product Design">
                        <div class="logo_container">
                            <span class="white"></span>
                            <span class="over"></span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
);

const Header = () => (
    <header>
        <a class="logo" href="https://www.loba.pt/pt/" title="Loba">
        <img src="https://www.loba.pt/images/loba.png" alt="Loba" width="110" height="28" />
        </a>
        <div class="logo_mobile">
            <a href="https://www.loba.pt/pt/" title="Loba">
                <img src="https://www.loba.pt/images/loba_mobile.png" alt="Loba" width="109" height="28" />
            </a>
        </div>

        <div class="top">
            <div class="mercado">
                <a href="#" title="Mercado" class="select"></a>
                <ul>
                    <li><a href="https://www.loba.pt/fr/" title="França">França</a></li>
                    <li><a href="http://www.loba.net.br" target="_blank" title="Brasil">Brasil</a></li>
                    <li><a href="http://www.loba.agency" target="_blank" title="Internacional">International</a></li>
                </ul>
            </div>
            <div id="brand_change">
                <button class="brands" />
            </div>
            <a href="#" title="Pesquisar" alt="Pesquisar" class="pesquisar" />
        </div>

        <nav>
            <ul class="menu">
                <li><a href="https://www.loba.pt/blog/" title="Blog">Blog</a></li>
                <li><a href="https://www.loba.pt/pt/sobre-nos" title="Sobre nós">Sobre nós</a></li>
                <li><a href="https://www.loba.pt/pt/contactos" title="Contactos">Contactos</a></li>
            </ul>
        </nav>
    </header>
);

const AreasNegocio = () => (
    <div id="areas_negocio" class="brands">
        <ul class="brand">
            <li class="brands selected"><a href="https://www.loba.pt/pt/brands" title="" /></li>
        </ul>
        <ul class="menu_brands">
            <li class="selected"><a href="https://www.loba.pt/pt/brands/portfolio-brands" title="Portfólio">Portfólio</a></li>
            <li><a href="https://www.loba.pt/pt/brands/branding" title="Branding ">Branding </a></li>
            <li><a href="https://www.loba.pt/pt/brands/comunicacao" title="Comunicação ">Comunicação </a></li>
            <li><a href="https://www.loba.pt/pt/brands/promocao" title="Promoção ">Promoção </a></li>
        </ul>
    </div>
);

const BrandschoiceMobile = () => (
    <div id="brandschoice_mobile">
        <div class="choose brands">
            <img src="https://www.loba.pt/images/brands_cor.png" alt="brand" width="136" height="23" />
        </div>
    </div>
);

const Menu = () => (
    <section id="menu" class="area">
        <nav>
            <ul class="nav brands">
                <li><a href="https://www.loba.pt/pt/sobre-nos" title="Sobre nós">Sobre nós</a>
                </li>
            </ul>
            <ul class="areas brands">
                <li class="selected"><a href="https://www.loba.pt/pt/brands/portfolio-brands" title="Portfólio">Portfólio</a>
                </li>
                <li><a href="https://www.loba.pt/pt/brands/branding" title="Branding ">Branding </a></li>
                <li><a href="https://www.loba.pt/pt/brands/comunicacao" title="Comunicação ">Comunicação </a></li>
                <li><a href="https://www.loba.pt/pt/brands/promocao" title="Promoção ">Promoção </a></li>
            </ul>
            <ul class="nav">
                <li><a href="https://www.loba.pt/blog/" title="Blog">Blog</a>
                </li>
                <li><a href="https://www.loba.pt/pt/contactos" title="Contactos">Contactos</a>
                </li>
            </ul>
        </nav>
    </section>
);

/* const Page = () => (
    <div id="page" class="widescreen">
    <div id="page_in" class="no_paddingbottom">
      <article id="portfolio">
        <div class="notWideContent">
          <hgroup>
            <h1>Gelados de Portugal</h1>
          </hgroup>
          <div class="navegacao desktop">
            <a href="https://www.loba.pt/pt/brands/portfolio-brands/portfolio-redes-sociais" title="" class="listagem" />
            <a href="https://www.loba.pt/pt/brands/portfolio-brands/portfolio-redes-sociais/cerveja-vadia" class="seguinte" />
          </div>

          <div id="detalhes_portfolio">Cliente . <strong>Gelados de Portugal</strong> / ANO . <strong>2018 </strong> /
            Trabalho . <strong>Redes Sociais</strong>
          </div>
        </div>

        <div id="imagem_big_portfolio" class="banner">
          <div id="wrapperIMG">
            <img id="" src="https://www.loba.pt/image_temp/1920X980_1920X980_370757e1d84479ca6de6b9635c776992.jpg"
              alt="Gelados de Portugal" />
          </div>
        </div>
        <div class="notWideContent margens">
          <div class="resumo">
            <p>Há projetos que são uma delícia! Gelado com sabor a ovos moles ou Pastel de Nata? Nós provámos e ficámos
              fãs!</p>
          </div>
          <div class="texto" style="column-gap: 4.2%;">
            <p>A Gelados de Portugal é uma marca portuguesa que se distingue pela promoção de sabores originais
              portugueses utilizando preferencialmente matéria prima portuguesa. Com a gestão das redes sociais&nbsp;
              (Facebook, Instagram) muitos ficaram rendidos a estes Gelados com sabores tipicamente portugueses. Usámos
              uma comunicação descontraída com publicações cheias de cor e humor, fizemos ofertas e passatempos,
              promovemos a história e histórias, eventos e datas felizes para atrair seguidores e consumidores.</p>
          </div>
        </div>
        <ul id="imagens_portfolio">
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X921_1920X921_d1713bc6ecf4df50dc4c2676ab084612.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X842_1920X842_1d40103929d2700d18083131593a6d1d.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X813_1920X813_0e629009d47eb25a1232b5e754075a45.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X1330_1920X1330_ec8efa79b27b258c686f30a22ef48386.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X1659_1920X1659_a185af9c5cedf4b312d73ddfdac95dc7.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X1605_1920X1605_22f60d3574f41e42bd21a81a01d1261f.jpg"
              alt="Gelados de Portugal"/>
          </li>
          <li class="total">
            <img src="https://www.loba.pt/image_temp/1920X978_1920X978_3df21dd514f084daff99d7a200a154f1.jpg"
              alt="Gelados de Portugal"/>
          </li>
        </ul>
        <div class="notWideContent">
          <div id="share">
            <span class="titulo">Share this</span>
            <a href="http://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.loba.pt%2Fpt%2Fbrands%2Fportfolio-brands%2Fportfolio-redes-sociais%2Fgelados-de-portugal"
              target="_blank" title="">Facebook./>
            <a href="http://twitter.com/share?text=Gelados de Portugal&amp;url=https%3A%2F%2Fwww.loba.pt%2Fpt%2Fbrands%2Fportfolio-brands%2Fportfolio-redes-sociais%2Fgelados-de-portugal"
              target="_blank" title="">Twitter./>
          </div>
          <div class="navegacao mobile">
            <a href="https://www.loba.pt/pt/brands/portfolio-brands/portfolio-redes-sociais" title=""
              class="listagem"/>
            <a href="https://www.loba.pt/pt/brands/portfolio-brands/portfolio-redes-sociais/cerveja-vadia"
              class="seguinte"/>
          </div>
        </div>
      </article>
    </div>

    <div class="experiencia">
      <span>Deseja dar forma ao seu produto?</span><a href="http://www.loba.pt/pt/contactos/contacte-nos"
        title="Desafie-nos">Desafie-nos</a> </div>
  />
);

const Footer = () => (
        // <a href="#" id="backTop" title="" style="right: 33.2px;"></a>

        <div class="footer">
            <div class="departamento">
                <div class="coluna">
                    <span class="local">Vendas</span>
                    <span class="tel large">
                        T. +351 256 197 889<br/> E. <a href="mailto:comercial@loba.pt"
                            title="">comercial@loba.pt</a></span>
                </div>
                <div class="coluna">
                    <span class="local">Suporte</span>
                    <span class="tel large">
                        T. +351 308 811 185<br/> E. <a href="mailto:suporte@loba.pt" title="">suporte@loba.pt</a></span>
                </div>
                <div class="coluna">
                    <span class="local">Geral</span>
                    <span class="tel large">
                        T. +351 256 668 413<br> E. <a href="mailto:geral@loba.pt" title="">geral@loba.pt</a></span>
                </div>
            </div>
            <div class="departamento newsletter">
                <span class="local">mailing list</span>
                <span class="titulo"></span>
                <span class="resumo">Queremos manter a ligação consigo.<br>Subscreva!</span>
                <form id="newsletter" method="post" action="https://azmail.globaz.net/subscribe.php"
                    accept-charset="UTF-8" siq_id="autopick_5016">
                    <input type="hidden" name="FormValue_Fields[CustomField1309]" value="pt"/>
                    <input name="FormValue_ListID" value="897" type="hidden"/>
                    <input name="FormValue_Command" value="Subscriber.Add" id="FormValue_Command" type="hidden"/>
                    <div class="text">
                        <label for="newsletter_email">Email</label>
                        <input required="" type="email" value="" name="FormValue_Fields[EmailAddress]"
                            id="FormValue_EmailAddress">
                        <input type="submit" value="">
                    </div>

                    <label class="checkbox" for="dados_newsletter"><input required="" type="checkbox" name="dados"
                            id="dados_newsletter" value="1">Autorizo a LOBA.CX a armazenar os meus dados pessoais com a
                        propósito de responder à minha solicitação de contato. As informações enviadas serão tratadas de acordo com o
                        regulamento de proteção de dados.</label>
                    <label class="checkbox" for="politica_newsletter"><input required="" type="checkbox" name="politica"
                            id="politica_newsletter" value="1">Li e aceito a <a href="/pt/politica-de-privacidade"
                            title="Política de Privacidade" target="_blank">Política de Privacidade</a> e <a
                            target="_blank" href="/pt/politica-de-cookies" title="Política de Cookies">Política de
                            Cookies</a>.</label>
                </form>

            </div>
        </div>

        <div class="rodape float">
            <div class="apoios">
                <a target="_blank" href="./downloads/Fichaprojeto.pdf">
                    <img src="./images/logos_rodape.png" alt="Logos" title="Logos" width="303" height="41"/>
                </a>
            </div>
        </div>

        <div class="rodape">
            <div class="redes_sociais">

                <a href="http://www.facebook.com/LOBA.cx" title="facebook" class="fb" target="_blank"></a>
                <a href="https://twitter.com/loba_cx" title="twitter" target="_blank"></a>
                <a href="https://vimeo.com/lobacx" title="vimeo" class="vimeo" target="_blank"></a>
                <a href="https://www.youtube.com/user/LOBAcx" title="youtube" class="youtube" target="_blank"></a>
                <a href="https://instagram.com/lobacx" title="instagram" class="instagram" target="_blank"></a>
                <a href="https://www.linkedin.com/company/loba-cx" title="linkedin" class="li" target="_blank"></a>
                <a href="https://www.pinterest.com/lobacx/" title="pinterest" class="pinterest" target="_blank"></a>

            </div>
            <div class="apoios">
                <a target="_blank" href="./downloads/Fichaprojeto.pdf">
                <img src="./images/logos_rodape.png" alt="Logos" title="Logos" width="303" height="41"/>
                </a>
            </div>
            <div class="ord">
                <nav>
                    <ul>
                        <li><a href="https://www.loba.pt/pt/politica-de-cookies" title="Política de Cookies">Política de
                                Cookies</a>
                        </li>
                        <li><a href="https://www.loba.pt/pt/politica-de-privacidade"
                                title="Política de Privacidade">Política de
                                Privacidade</a></li>
                        <li><a href="https://www.loba.pt/pt/carreiras-loba" title="Carreiras">Carreiras</a></li>
                    </ul>
                </nav>
                <span class="direitos">© 2020 LOBA . Todos os direitos reservados</span>
            </div>
        </div>
);  */

ReactDOM.render(
    <App />,
    document.getElementById("root")
);